#!/usr/bin/env python
#coding: utf8

import sys
import os
import tempfile
import subprocess
import shutil
import re

f = open(os.devnull, 'w')

def FCompression (i, temp) :
	subprocess.check_call(["./huff", i, temp+"/"+i+".huff", "-f"], stdout=f, shell=False)
	
def FDecompression (i) :
	nom = re.search("(.)+\.huff", i) 
	if nom :
		sortie = open(i[0:-5], 'w')
		subprocess.check_call(["./dehuff", i], stdout=sortie, shell=False)
		os.remove(i)

def parcours (temp, dossierBN, dossier, compression) :

	if (compression) :
		os.mkdir(temp+"/"+dossierBN)
	else :
		subprocess.check_call(["tar", "xvf", dossierBN], stdout=f)
		
		
	for root, dirs, files in os.walk(dossier):  
		
		if compression :
			for i in dirs :
					os.mkdir(temp+"/"+dossierBN+"/"+i)
		
		for i in files:
		
			if os.path.isdir(os.path.join(root, i)) == False :
			
				if compression :
					FCompression (os.path.join(root, i), temp)
				else :
					FDecompression	(os.path.join(root, i))
	
	if (compression) :
		rep = os.getcwd()
		os.chdir(temp)
		
		subprocess.check_call(["tar", "cvf", dossierBN+".tar.huff", dossierBN], stdout=f)

		if os.path.isfile(rep+"/"+dossierBN+".tar.huff"):
			os.remove(rep+"/"+dossierBN+".tar.huff")
		shutil.move(temp+"/"+dossierBN+".tar.huff", rep)

for i in range (0,1) :
	if re.search("-[xc]+", sys.argv[1+i]) :

		if re.search("-.*x.*", sys.argv[1+i]) :
			
			if i == 0 :
				fichier = sys.argv[2]
			else :
				fichier = sys.argv[1]
				
			if os.path.isfile(fichier) == False:
				print "Le fichier n'existe pas \n"
				print "Utilisation : python multiHuff.py -x nom_du_dossier_à_décompresser"
				sys.exit(0)
				
			dossier = re.search("(.)+\.tar.huff", fichier) 
			if dossier :
				parcours (None, fichier, fichier[0:-9], False)
			
			
			break
		
		elif re.search("-.*c.*", sys.argv[1+i]) :
		
			if i == 0 :
				dossier = sys.argv[2]
			else :
				dossier = sys.argv[1]
				
			
			if os.path.isdir(dossier) == False:
				print "Le dossier n'existe pas \n"
				print "Utilisation : python multiHuff.py -c nom_du_dossier_à_compresser"
				sys.exit(0)

			temp = tempfile.mkdtemp()
			dossierBN = os.path.basename(dossier)
				
			parcours (temp, dossierBN, dossier, True)
				
			shutil.rmtree(temp)
				
			break
		
	else :
		print("Les options utilisées sont incorrectes !")

	

