/**
 * \file probHuffman.c
 * \brief Contient les définitions des fonctions servant à calculer la distribution des caractères dans le fichier à compresser et à l'afficher
 * \authors Guillaume Pérution-Kihli et Judicaël Russo
 */

#include "probHuffman.h"

double* distribution (FILE *fg, int* totalCara)
{
	int c;
	*totalCara = 0;

	double* probabilite = calloc(256, sizeof(double*));
	if (probabilite == NULL)
	{
		fprintf(stderr, "Allocation impossible (variable probabilite, fonction distribution, fichier %s, ligne %i) \n", __FILE__, __LINE__);
		exit(EXIT_FAILURE);
	}

	while ((c = fgetc(fg)) != EOF)
	{
		probabilite[c]++;
		(*totalCara)++;
	}

	if (*totalCara == 0)
	{
		printf("Fichier vide ! Fin du programme !\n");
		exit(1);
	}

	for (int i = 0; i < 256; i++)
	{
		if (probabilite[i] > 0) probabilite[i] /= (*totalCara-1);
	}

	return probabilite;
}

void afficheDistrib (double* distrib)
{
	for (int i = 0; i < 256; i++)
	{
		if (distrib[i] != 0) 
		{
			printf("car : %c de code : %x de proba : %f \n", i, i, distrib[i]);
		}
	}
	printf("\n");
}
