/**
 * \file arbreHuff.h
 * \brief Contient les signatures des fonctions nécessaires à la construction de l'abre
 * \authors Guillaume Pérution-Kihli et Judicaël Russo
 */

#ifndef ARBREHUFFMAN_H
#define ARBREHUFFMAN_H
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include "Noeud.h"

/**
 * \fn int arbreHuff (double*, Noeud*, int*)
 * \brief Construit l'abre de Huffman
 * 
 * Fonction permettant la construction de l'abre de Huffman à partir des probabilités d'apparition des caractères
 *
 * \param probabilite : tableau de double, contenant la probabilité d'apparition de chaque caractère
 * \param arbre : tableau de Noeud, paramètre de résultat, doit contenir un tableau de Noeud vide de taille 511, contient à la fin d'exécution de la fonction l'arbre complet
 * \param correspondances : tableau d'entiers, paramètre de résultat, doit contenir un tableau d'entiers de taille 256 vide, contient à la fin d'exécution de la fonction un tableau de correspondances entre les numéros de caractères, en indice, et en valeur, l'indice correspondant dans l'arbre
 * 
 * \return un entier valant le nombre de caractères différents contenu dans le document
 */

int arbreHuff (double*, Noeud*, int*);

/**
 * \fn void min(Noeud*, int, int*, int*)
 * \brief Trouve dans l'arbre les indices des deux plus petites probabilités n'ayant pas de père
 * 
 * Fonction permettant de connaître les indices des deux noeuds de l'arbre ayant la probabilité la plus faible et n'ayant pas encore de père
 *
 * \param arbre : tableau de Noeud, représentant l'arbre de Huffman à construire et contenant déjà toutes les feuilles
 * \param taille : entier, taille de l'arbre au moment de l'appel de la fonction
 * \param iMin : entier, paramètre de résultat, contient l'indice de la plus petite probabilité de l'abre à la fin de l'exécution
 * \param iMin2 : entier, paramètre de résultat, contient l'indice de la deuxième plus petite probabilité de l'abre à la fin de l'exécution
 */

void min(Noeud*, int, int*, int*);

/**
 * \fn void afficheArbre (Noeud*, int, int)
 * \brief Affiche le contenu de l'arbre dans le terminal
 * 
 * Fonction permettant d'afficher le contenu de l'arbre dans le terminal, entre deux indices donnés
 * 
 * \param arbre : Tableau de Noeud, contenant l'arbre à afficher
 * \param debut : entier, contenant l'indice où commencer l'affichage, > 0
 * \param fin : entier, contenant l'indice où arrêter l'affichage, doit être inférieur à la taille réelle de l'arbre et supérieur à 0
 */

void afficheArbre (Noeud*, int, int);

/**
 * \fn void initArbre (Noeud*, int)
 * \brief Initialise les éléments pere, fg et fd des Noeuds à -1
 * 
 * \param arbre : Tableau de Noeud, contenant l'arbre à initialiser
 * \param taille : entier contenant la taille du tableau de Noeud arbre
 */

void initArbre (Noeud*, int);

/**
 * \fn Noeud* lireArbre (FILE*, int)
 * \brief Lit et reconstruit l'arbre dans le fichier à décompresser
 * 
 * Cette fonction lit les les noeuds de l'abre contenus dans le fichier à décompresser et reconstruit l'abre à partir de ceux-ci
 * 
 * \param fg : FILE, pointeur vers le fichier à lire, le curseur doit être positionné au début des noeuds dans le fichier 
 * \param nbCaracteresDiff : entier, valant le nombre de caractères de différents contenus dans le fichier qui a été compressé
 * 
 * \return tableau de Noeud, représentant l'arbre de Huffman
 */

Noeud* lireArbre (FILE*, int);

#endif
