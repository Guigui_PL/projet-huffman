/**
 * \file codeHuff.c
 * \brief Contient les definitions des fonctions générant et manipulant les codes représentant les caractères dans l'arbre de Huffman
 * \authors Guillaume Pérution-Kihli et Judicaël Russo
 */

#include "codeHuff.h"

LSDC* codeHuff (Noeud* arbre, int nbCaracteres)
{
	LSDC* codes = (LSDC*) malloc(nbCaracteres * sizeof(LSDC));
	if (codes == NULL)
	{
		fprintf(stderr, "Allocation impossible (variable codes, fonction codeHuff, fichier %s, ligne %i) \n", __FILE__, __LINE__);
		exit(EXIT_FAILURE);
	}

	if (nbCaracteres > 1)	
	{
		LSDC code = initLSDC();
		construitCode((nbCaracteres-1)*2, arbre, codes, code, nbCaracteres);
		supprimerLSDC(code);
	}
	else codes[0] = initLSDC(NULL, -1, NULL);
	
	return codes;
}

void construitCode (int i, Noeud* arbre, LSDC* codes, LSDC code, int nbCaracteres)
{
  if (i < nbCaracteres) codes[i] = code;
  else 
  {
    LSDC codeGauche = copieListe(code);
    insererFin(&codeGauche, 0);
    LSDC codeDroite = copieListe(code);
    insererFin(&codeDroite, 1);
    
    construitCode(arbre[i].fg, arbre, codes, codeGauche, nbCaracteres);
    construitCode(arbre[i].fd, arbre, codes, codeDroite, nbCaracteres);
  }
}

void afficherCodes(LSDC* codes, int* correspondances, int nbCaracteres)
{
	for (int i = 0; i < 256; i++)
	{
		if (correspondances[i] != -1)
		{
			printf("code[%i] = codeChar(%c) = ", correspondances[i], i);
			afficherLSDC(codes[correspondances[i]]);
			printf("\n");
		}
	}
}

void longueurMoyenne (int* correspondances, double* distrib, LSDC* codes, int nbCaracteresDiff)
{
	double total = 0;
	for (int i = 0; i < 256; i++)
	{
		if (correspondances[i] != -1) total += distrib[i] * tailleLSDC(codes[correspondances[i]]);
	}
	printf("Longueur moyenne de codage : %.2f au lieu de 8 !\n", total);
}

void desallocTCB (LSDC* codes, int taille)
{
	for (int i = 0; i < taille; i++)
	{
		supprimerLSDC(codes[i]);
	}
	free(codes);
	codes = NULL;
}