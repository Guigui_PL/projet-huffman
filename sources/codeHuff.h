/**
 * \file codeHuff.h
 * \brief Contient les signatures des fonctions générant et manipulant les codes représentant les caractères dans l'arbre de Huffman
 * \authors Guillaume Pérution-Kihli et Judicaël Russo
 */

#ifndef CODEHUFF_H
#define CODEHUFF_H
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include "Noeud.h"
#include "LSDC.h"

/**
 * \fn LSDC* codeHuff (Noeud*, int)
 * \brief Génère le tableau de codes binaires représentant chaque caractère
 *
 * \param arbre : Tableau de Noeud représentant l'arbre de Huffman déjà construit
 * \param nbCaracteres : entier, valant le nombre de caractères différents représentés dans l'arbre
 *
 * \return Tableau de Listes doublement chaînées contenant les codes de chaque caractère
 */ 

LSDC* codeHuff (Noeud*, int);

/**
 * \fn void afficherCodes(LSDC*, int*, int)
 * \brief Affiche dans la sortie standard tous les codes de Huffman
 * 
 * Affiche dans la sortie standard tous les codes de Huffman contenu dans un tableau de listes doublement chaînées et le caractère qu'ils représentent
 * 
 * \param codes : Tableau contenant les codes binaires de chaque caractère
 * \param correspondances : Tableau d'entier faisant le lien entre chaque indice du tableau codes et le caractère qu'il représente
 * \param nbCaracteres : entier, valant le nombre de caractères différents représentés dans l'abre
 */

void afficherCodes(LSDC*, int*, int);

/** 
 * \fn void longueurMoyenne (int*, double*, LSDC*, int)
 * \brief Affiche dans la sortie standard la longueur moyenne de codage et le gain 
 * 
 * Affiche la longueur moyenne du codage, la taille du fichier initial et celle du fichier de destination ainsi que le gain en pourcent dans la sortie standard
 * 
 * \param correspondances : Tableau d'entier faisant le lien entre chaque indice du tableau codes et le caractère qu'il représente
 * \param distrib : Tableau contenant la probabilité d'apparition de chaque caractère dans le fichier initial
 * \param codes : Tableau contenant les codes binaires de chaque caractère
 * \param nbCaracteresDiff : entier, valant le nombre de caractères différents représentés dans l'arbre
 */

void longueurMoyenne (int*, double*, LSDC*, int);

/**
 * \fn void construitCode (int, Noeud*, LSDC*, LSDC, int)
 * \brief Génère les codes binaires
 * 
 * Génère les codes binaires en parcourant récusirvement l'abre de Huffman
 * 
 * \param i : entier, indice de la position dans le tableau représentant l'abre
 * \param arbre : tableau de Noeud, représentant l'abre de Huffman totalement construit
 * \param codes : LSDC*, pointeur vers le tableau de codes à construire
 * \param code : LSDC, représentant le code de chaque caractère en cours de construction
 * \param nbCaracteres : entier, valant le nombre de caractères différents représentés dans l'arbre
 */

void construitCode (int, Noeud*, LSDC*, LSDC, int);

/**
 * \fn void desallocTCB (LSDC*, int)
 * \brief Désalloue proprement la mémoire allouée au tableau de codes binaires
 * 
 * \param codes : tableau de listes doublement chaînées, tableau de codes qui va être désalloué
 * \param taille : entier, valant la taille du tableau de LSDC à désallouer
 */

void desallocTCB (LSDC*, int);

#endif