/**
 * \file probHuffman.h
 * \brief Contient les signatures des fonctions servant à calculer la distribution des caractères dans le fichier à compresser et à l'afficher
 * \authors Guillaume Pérution-Kihli et Judicaël Russo
 */

#ifndef PROBHUFFMAN_H
#define PROBHUFFMAN_H
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>

/**
 * \fn double* distribution (FILE*, int*)
 * \brief Calcule la distribution de chaque caractères
 * 
 * \param fg : FILE*, adresse vers le flux de lecture du fichier à compresser, avec le curseur placé au début
 * \param totalCara : entier, donnée-résultat, contenant à la fin de l'exécution de la fonction le nombre total de caractères contenus dans le fichier
 * 
 * \return tableau de double, contenant la probabilité d'apparition de chaque caractère
 */

double* distribution (FILE*, int*);

/**
 * \fn void afficheDistrib (double*)
 * \brief affiche la distribution des différents caractères 
 * 
 * \param distrib : tableau de double, contenant la probabilité d'apparition de chaque caractère 
 */

void afficheDistrib (double*);

#endif
