/**
 * \file writeHuff.c
 * \brief Contient la définition de la fonction servant à écrire le fichier compressé avec huff
 * \authors Guillaume Pérution-Kihli et Judicaël Russo
 */

#include "writeHuff.h"

int writeHuff(FILE* fp, FILE* fg, double* proba, Noeud* arbre, LSDC* codes, int* correspondances, int totalCara, int nbCaracteres)
{
	// On commence par écrire le nombre total de caractères du fichier de base, sur 32 bits
	fwrite(&totalCara, 4, 1, fp);

	// On écrit ensuite le nombre de caractères différent sur 8 bits
	// On écrit 0 si il y a 256 caractères différents
	if (nbCaracteres != 256) fputc(nbCaracteres, fp);
	else fputc(0, fp);

	int nbOctets = 5;

	// Ensuite, on écrit la liste des caractères, sur 8 bits chacun
	for (int i = 0; i < 256; i++)
	{
		if (proba[i] != 0) 
		{
			fputc(i, fp);
			nbOctets++;
		}
	}

	// On écrit les branches gauches puis droites de l'arbre chacune sur 16 bits
	if (nbCaracteres > 1)
	{
		int i = nbCaracteres-1;
		do
		{
			i++;
			fwrite(&(arbre[i].fg), 2, 1, fp);
			nbOctets += 2;
		} while (arbre[i].pere != -1);

		i = nbCaracteres-1;
		do
		{
			i++;
			fwrite(&(arbre[i].fd), 2, 1, fp);
			nbOctets += 2;
		} while (arbre[i].pere != -1);

		// Enfin, on écrit nos caractères encodés à partir du fichier de départ
		rewind(fg);

		LSDC listeCodes = initLSDC();
		LSDC code = NULL;
		int *tailles = calloc(nbCaracteres, sizeof(int*));

		int c, tailleC = 0;

		c = fgetc(fg);

		while (!feof(fg))
		{
			c = correspondances[c];
			insererFin(&listeCodes, c);
			if (tailles[c] == 0) tailles[c] = tailleLSDC(codes[c]);
			tailleC += tailles[c];

			while (tailleC >= 8)
			{
				fputc(codeToDec(&listeCodes, codes, &code), fp);
				nbOctets++;
				tailleC -= 8;
			}
			c = fgetc(fg);
		}

		while (tailleC > 0)
		{
			fputc(codeToDec(&listeCodes, codes, &code), fp);
			tailleC -= 8;
			nbOctets++;
		}
		
		free(tailles);
		tailles = NULL;
	}

	return nbOctets;
}
