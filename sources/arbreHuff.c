/**
 * \file arbreHuff.c
 * \brief Contient les definitions des fonctions nécessaires à la construction de l'abre
 * \authors Guillaume Pérution-Kihli et Judicaël Russo
 */

#include "arbreHuff.h"

int arbreHuff (double* probabilite, Noeud* arbre, int* correspondances)
{
	int cpt = 0;
	int iMin, iMin2;
	int nbCaracteres;

	for (int i = 0; i < 256; i++)
	{
		if (probabilite[i] != 0)
		{
			arbre[cpt].freq = probabilite[i];
			correspondances[i] = cpt;
			cpt++;
		}
		else correspondances[i] = -1;
	}
	nbCaracteres = cpt;

	int debut = cpt;
	cpt--;
	if (nbCaracteres > 1)
	{
		while (arbre[cpt].freq < 1 && cpt < 511)
		{
			cpt++;
			min(arbre, cpt, &iMin, &iMin2);
		
			arbre[cpt].fg = iMin;
			arbre[iMin].pere = cpt;

			arbre[cpt].fd = iMin2;
			arbre[iMin2].pere = cpt;

			arbre[cpt].freq = arbre[iMin].freq + arbre[iMin2].freq;
		}

	}
	afficheArbre (arbre, 0, debut-1);
	if (nbCaracteres > 1) afficheArbre (arbre, debut, cpt);

	return nbCaracteres;
}

void min(Noeud* arbre, int taille, int* iMin, int* iMin2)
{
	double min = 1;
	double min2 = 1;

	for (int i = 0; i < taille; i++)
	{
		if (min > arbre[i].freq && arbre[i].pere == -1)
		{
			min2 = min;
			*iMin2 = *iMin;
			min = arbre[i].freq;
			*iMin = i;
		}
		else if (min2 > arbre[i].freq && arbre[i].pere == -1)
		{
			min2 = arbre[i].freq;
			*iMin2 = i;
		}
	}
}

void afficheArbre (Noeud* arbre, int debut, int fin)
{
	printf("ind \t pere \t fg \t fd \t freq \n");
	for (int i = debut; i <= fin; i++)
	{
		printf("%i \t %i \t %i \t %i \t %f \n", 
		i, arbre[i].pere, arbre[i].fg, arbre[i].fd, arbre[i].freq);
	}
	printf("-------------- \n \n");
}

void initArbre (Noeud* arbre, int taille)
{
	for (int i = 0; i < taille; i++)
	{
		arbre[i].pere = -1;
		arbre[i].fg = -1;
		arbre[i].fd = -1;
	}
}

Noeud* lireArbre (FILE* fg, int nbCaracteresDiff)
{
	Noeud* arbre = (Noeud*) malloc(511 * sizeof(Noeud));
	if (arbre == NULL)
	{
		fprintf(stderr, "Allocation impossible (variable arbre, fonction lireArbre, fichier %s, ligne %i) \n", __FILE__, __LINE__);
		exit(EXIT_FAILURE);
	}

	initArbre(arbre, 511);
	int c1, c2;

	for (int i = nbCaracteresDiff; i <= (nbCaracteresDiff-1)*2; i++)
	{
		c1 = fgetc(fg);
		c2 = fgetc(fg);
		arbre[i].fg = c1 + c2 * 256;
	}

	for (int i = nbCaracteresDiff; i <= (nbCaracteresDiff-1)*2; i++)
	{
		c1 = fgetc(fg);
		c2 = fgetc(fg);
		arbre[i].fd = c1 + c2 * 256;
	}

	return arbre;
}
