/**
 * \file lireCara.h
 * \brief Contient les signatures des fonctions servant à lire et décompresser les caractères contenu dans le fichier à décompresser
 * \authors Guillaume Pérution-Kihli et Judicaël Russo
 */

#ifndef LIRECARA_H
#define LIRECARA_H
#include <stdlib.h>
#include <stdio.h>
#include "Noeud.h"

/**
 * \fn void lireCara(FILE*, Noeud*, int*, int, int)
 * \brief Lit et décompresse les caractères contenu dans le fichier compressé
 * 
 * \param fg : FILE*, contenant l'adresse du fichier compressé, avec le curseur placé au niveau du code représentant les caractères
 * \param arbre : tableau de Noeud, représentant l'arbre de Huffman complet
 * \param correspondances : tableau d'entiers, permettant de faire le lien entre l'indice d'une feuille de l'abre et le caractère qu'il représente
 * \param nbCaracteresTotal : entier, valant le nombre total de caractères du fichier une fois décompressé
 * \param nbCaracteresDiff : entier, valant le nombre de caractères différents représentés dans l'abre
 */

void lireCara(FILE*, Noeud*, int*, int, int);

/**
 * \fn void decToByte (int, int*)
 * \brief Transforme un nombre entier décimal < 256 en représentation binaire 
 * 
 * Transforme un nombre entier (naturel) décimal inférieur à 256 en un tableau d'entiers contenant 0 ou 1 dans chaque cellule et représentant un octet
 * 
 * \param decimal : entier, compris entre 0 et 256
 * \param octet : tableau d'entiers, donnée-résultat, contenant la représentation binaire du nombre décimal à la fin de l'exécution de la fonction
 */

void decToByte (int, int*);

#endif
