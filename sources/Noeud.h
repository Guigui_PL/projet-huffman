/**
 * \file Noeud.h
 * \brief Contient la définition de la strucutre Noeud, qui représente un noeud de l'arbre de Huffman
 * \authors Guillaume Pérution-Kihli et Judicaël Russo
 */

#ifndef NOEUD_H
#define NOEUD_H

/**
 * structure représentant un noeud de l'abre de Huffman
 */

typedef struct noeud 
{
	int pere; /**< Indice vers le père du noeud, -1 si pas de père */
	int fg; /**< Indice vers le fils gauche, -1 si pas de fg */
	int fd; /**< Indice vers le fils droite, -1 si pas de fd */
	double freq; /**< Probabilité du nœud */
} Noeud;

#endif
