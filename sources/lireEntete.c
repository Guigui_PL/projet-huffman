/**
 * \file lireEntete.c
 * \brief Contient la définition de la fonction servant lire l'entête d'un fichier compressé avec le compresseur huff
 * \authors Guillaume Pérution-Kihli et Judicaël Russo
 */

#include "lireEntete.h"

int* lireEntete (FILE* fg, int* nbCaracteresDiff, int* nbCaracteresTotal)
{
	// On lit le nbCaracteresDiff écrit sur 4 octets
	int* octets = (int*) malloc (4 * sizeof(int));
	for (int i = 0; i < 4; i++) octets[i] = fgetc(fg);

	*nbCaracteresTotal = 0;

	for (int i = 3; i >= 0; i--)
		*nbCaracteresTotal = *nbCaracteresTotal * 256 + octets[i];

	// On récupère le nombre de caractère différents, stocké sur un octet
	*nbCaracteresDiff = fgetc(fg);
	if (*nbCaracteresDiff == 0) *nbCaracteresDiff = 256;

	// On récupère la liste des caractères avec pour indice l'indice qu'ils auront dans l'arbre
	int *correspondances = NULL;
	correspondances = (int*) malloc(*nbCaracteresDiff * sizeof(int));
	if (correspondances == NULL)
	{
		fprintf(stderr, "Allocation impossible (variable correspondances, fonction lireEntete, fichier %s, ligne %i) \n", __FILE__, __LINE__);
		exit(EXIT_FAILURE);
	}

	for (int i = 0; i < *nbCaracteresDiff; i++)
		correspondances[i] = fgetc(fg);

	return correspondances;

}
