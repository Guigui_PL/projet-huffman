/**
 * \file LSDC.c
 * \brief Contient les definitions des fonctions manipulant la structure LSDC (Liste doublement chaînée)
 * \authors Guillaume Pérution-Kihli et Judicaël Russo
 */

#include "LSDC.h"

LSDC initLSDC()
{
	return creerLSDC (NULL, -1, NULL);
}

LSDC creerLSDC (LSDC precedent, int entier, LSDC suivant)
{
	LSDC lsdc = (LSDC) malloc(sizeof(LSDC));
	if (lsdc == NULL)
	{
		fprintf(stderr, "Allocation impossible (variable lsdc, fonction creerLSDC, fichier %s, ligne %i) \n", __FILE__, __LINE__);
		exit(EXIT_FAILURE);
	}
	
	lsdc->entier = entier;
	
	if (precedent != NULL) lsdc->precedent = precedent;
	else lsdc->precedent = lsdc;
	  
	if (suivant != NULL) lsdc->suivant = suivant;
	else lsdc->suivant = lsdc;
	
	return lsdc;
}

int estVide(LSDC lsdc)
{
	return lsdc == lsdc->suivant;
}

int tailleLSDC (LSDC lsdc)
{
	lsdc = lsdc->suivant;
	int cpt = 0;
	while (lsdc->entier != -1) 
	{
		cpt++;
		lsdc = lsdc->suivant;
	}
	return cpt;
}

void insererApres (LSDC* lsdc, LSDC prec, int entier)
{
	LSDC entierInsere = creerLSDC(prec, entier, prec->suivant);
	prec->suivant->precedent = entierInsere;
	prec->suivant = entierInsere;
}

void insererFin (LSDC* lsdc, int entier)
{
	insererApres(lsdc, ((*lsdc)->precedent), entier);
}

void supprimerCell (LSDC* lsdc, LSDC suppr)
{
	if (suppr->suivant == suppr->precedent)
	{
		(*lsdc)->suivant = (*lsdc);
		(*lsdc)->precedent = (*lsdc);
	}
	else
	{
		suppr->precedent->suivant = suppr->suivant;
		suppr->suivant->precedent = suppr->precedent;
	}
	free(suppr);
	suppr = NULL;
}

int retirerEntierDebut (LSDC* lsdc)
{
	int entier = (*lsdc)->suivant->entier;
	supprimerCell(lsdc, (*lsdc)->suivant);
	return entier;
}

void supprimerLSDC (LSDC lsdc)
{
	LSDC tmp = lsdc->suivant;
	LSDC tmpSuivant;
 
	while(tmp->entier != -1)
	{
		tmpSuivant = tmp->suivant;
		free(tmp);
		tmp = tmpSuivant;
	}
	
	free(tmp);
	tmp = NULL;
}

void afficherLSDC (LSDC lsdc)
{
	lsdc = lsdc->suivant;
	while (lsdc->entier != -1)
	{
		printf("%i", lsdc->entier);
		lsdc = lsdc->suivant;
	}
}

int codeToDec (LSDC* liste, LSDC* codes, LSDC *code)
{
	int decimal = 0;
	
	for (int i = 0; i < 8; i++)
	{
		if (*code == NULL)
		{
			if (estVide(*liste)) decimal *= 2;
			else
			{
				*code = codes[retirerEntierDebut(liste)];
				*code = CellSuivante(code);
				decimal = decimal * 2 + (*code)->entier;
				*code = CellSuivante(code);
			}
		}
		else 
		{
			decimal = decimal * 2 + (*code)->entier;
			*code = CellSuivante(code);
		}
	}
	return decimal;
}

LSDC CellSuivante (LSDC* liste)
{
	if ((*liste)->suivant->entier == -1) return NULL;
	else return (*liste)->suivant;
}

LSDC copieListe (LSDC lsdc)
{
	LSDC copie = initLSDC();
	lsdc = lsdc->suivant;
	while (lsdc->entier != -1)
	{
		insererFin(&copie, lsdc->entier);
		lsdc = lsdc->suivant;
	}
	return copie;
}
