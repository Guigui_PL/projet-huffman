/**
 * \file lireCara.c
 * \brief Contient les définitions des fonctions servant à lire et décompresser les caractères contenu dans le fichier à décompresser
 * \authors Guillaume Pérution-Kihli et Judicaël Russo
 */

#include "lireCara.h"

void lireCara (FILE* fg, Noeud* arbre, int* correspondances, int nbCaracteresTotal, int nbCaracteresDiff)
{
	if (nbCaracteresDiff > 1)
	{
		int racine = (nbCaracteresDiff-1)*2;
		int c, cpt = 0, indice = racine;
		int* octet = malloc(8 * sizeof(int*));
		
		while ((c = fgetc(fg)) != EOF)
		{
			decToByte(c, octet);
			cpt = 0;
		
			while (cpt < 8 && nbCaracteresTotal != 0)
			{
				if (octet[cpt]) indice = arbre[indice].fd;
				else indice = arbre[indice].fg;

				if (indice < nbCaracteresDiff) 
				{
					printf("%c", correspondances[indice]);
					indice = racine;
					nbCaracteresTotal--;
				}

				cpt++;
			}

		}
		
		free(octet);
		octet = NULL;
	}
	else
	{
		for (int i = 0; i < nbCaracteresTotal; i++)
		{
			printf("%c", correspondances[0]);
		}
	}
}

void decToByte (int decimal, int* octet)
{
	for (int i = 7; i >= 0; i--)
	{
		octet[i] = decimal%2;
		decimal /= 2;
	}
}