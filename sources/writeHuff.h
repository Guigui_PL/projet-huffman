/**
 * \file writeHuff.h
 * \brief Contient la signature de la fonction servant à écrire le fichier compressé avec huff
 * \authors Guillaume Pérution-Kihli et Judicaël Russo
 */

#ifndef WRITEHUFF_H
#define WRITEHUFF_H
#include <stdlib.h>
#include <stdio.h>
#include "Noeud.h"
#include "LSDC.h"

/**
 * \fn int writeHuff(FILE*, FILE*, double*, Noeud*, LSDC*, int*, int, int)
 * \brief
 * 
 * \param fp : FILE*, fichier (vide) dans lequel on va écrire
 * \param fg : FILE*, fichier dans lequel on lit les caractères à compresser, le curseur doit être placé au début
 * \param proba : tableau de double, contenant la probabilité d'apparition de chaque caractère 
 * \param arbre : tableau de Noeud, contenant l'arbre de Huffman
 * \param codes : LSDC, contenant les codes représentant chaque caractère
 * \param correspondances : tableau d'entiers, faisant la correspondance entre l'entier représentant chaque caractère et l'indice de la feuille de l'abre le représentant
 * \param totalCara : entier, valant le nombre total de caractères contenus dans le fichier de départ
 * \param nbCaracteres : entier, valant le nombre de caractères différents représentés dans l'abre
 * 
 * \return entier, valant le nombre total d'octets écrits dans le fichier compressé
 */

int writeHuff(FILE*, FILE*, double*, Noeud*, LSDC*, int*, int, int);

#endif
