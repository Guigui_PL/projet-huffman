/**
 * \file LSDC.h
 * \brief Contient la structure LSDC et les signatures des fonctions manipulant la structure LSDC
 * \authors Guillaume Pérution-Kihli et Judicaël Russo
 */

#ifndef LSDC_H
#define LSDC_H
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>

/**
 * \brief structure représentant une cellule d'une LSDC (liste doublement chaînées)
 *
 * il s'agit d'une cellule d'une liste doublement chaînée \n
 * La cellule ayant pour valeur l'entier -1 est la cellule déclarant le début de la liste, elle ne contient pas de donnée (une liste ne contenant que cette cellule est vide) \n
 * cell n'est conçu que pour représenter un entier positif \n
 * cell.precedent et cell.suivant contiennent l'adresse de la cellule précédent et suivante - si la liste est vide, ils contiennent l'adresse de la cellule elle-même et cell.entier vaut -1
 */

typedef struct cell
{
	struct cell *precedent; /**< Adresse de la cellule précédente */
	int entier; /**< Valeur de la cellule, > 0 ou = -1 si première cellule */
	struct cell *suivant; /**< Adresse de la cellule suivante */
} Cell;

/**
 * \typedef Cell * LSDC 
 * \brief liste doublement chaînée 
 * 
 * Le type LSDC est une liste doublement chaînée contenant des entiers (positifs). Les cellules de la liste sont des structures cell Cell.
 */

typedef Cell * LSDC;

/**
 * \fn LSDC initLSDC()
 * \brief Initialise une liste doublement chaînée
 * 
 * Fonction permettant l'initialisation d'un LSDC, avec les valeurs que la première cellule doit contenir : l'élement entier doit contenir -1, et suivant ainsi que précédent doivent contenir l'adresse de la liste elle-même 
 * 
 * \return un LSDC vide
 */

LSDC initLSDC();

/**
 * \fn LSDC creerLSDC (LSDC, int, LSDC)
 * \brief Créer une nouvelle cellule d'une liste LSDC
 * 
 * Fonction permettant d'ajouter une cellule à un LSDC qui a été initialisé
 * 
 * \param precedent : adresse de la cellule précédente (ou NULL si la liste ne contient encore aucune cellule)
 * \param entier : valeur de l'entier
 * \param suivant : adresse de la cellule suivante (ou NULL si la liste ne contient encore aucune cellule)
 * 
 * \return L'adresse de la cellule ajoutée de type LSDC
 */

LSDC creerLSDC (LSDC, int, LSDC);

/**
 * \fn int estVide(LSDC)
 * \brief Permet de savoir si un LSDC est vide
 * 
 * \param lsdc : un LSDC initialisé
 * 
 * \return un booléen (int) valant true (1) si le LSDC est vide, false (0) sinon
 */

int estVide(LSDC);

/**
 * \fn int tailleLSDC (LSDC)
 * \brief Nombre de cellules contenues dans la liste
 * 
 * Cette fonction compte le nombre de cellules contenues dans une LSDC 
 * 
 * \param lsdc : un LSDC initialisé 
 * 
 * \return un entier, valant le nombre de cellules de la LSDC
 */

int tailleLSDC (LSDC);

/**
 * \fn void insererApres (LSDC*, LSDC, int)
 * \brief Insère un eniter après un autre
 * 
 * Fonction permettant d'insérer un entier après une cellule d'une LSDC  initialisé 
 * 
 * \param lsdc : LSDC initialisé 
 * \param prec : adresse de cellule après laquelle on ajoute un entier 
 * \param entier : valeur de l'entier ajouté
 */

void insererApres (LSDC*, LSDC, int);

/**
 * \fn void insererFin (LSDC*, int)
 * \brief Insère un entier à la fin d'une liste
 * 
 * \param lsdc : LSDC initialisé 
 * \param entier : valeur de l'entier ajouté
 */

void insererFin (LSDC*, int);

/**
 * \fn void supprimerCell (LSDC*, LSDC)
 * \brief Supprime une cellule d'une LSDC
 * 
 * \param lsdc : LSDC, adresse du début de la liste doublement chaînée
 * \param suppr : LSDC, adresse de la cellule à supprimer
 */

void supprimerCell (LSDC*, LSDC);

/**
 * \fn int retirerEntierDebut (LSDC*)
 * \brief retire le premier entier d'un LSDC et retourne sa valeur 
 * 
 * \param lsdc : LSDC non vide dont on retire le premier élément 
 * 
 * \return entier, valant la valeur supprimée
 */

int retirerEntierDebut (LSDC*);

/**
 * \fn void supprimerLSDC(LSDC)
 * \brief Supprime et désalloue un LSDC 
 * 
 * Cette fonction sert à supprimer totalement un LSDC, y compris la première cellule dont la variable entier vaut -1, et procède à la désallocation complète du LSDC. \n
 * Le LSDC devient un LSDC non initialisé.
 * 
 * \param lsdc : le LSDC initialisé à supprimer et désallouer
 */

void supprimerLSDC(LSDC);

/**
 * \fn void afficherLSDC (LSDC)
 * \brief affiche un LSDC dans la sortie standard
 * 
 * \param lsdc : le LSDC à afficher
 */

void afficherLSDC (LSDC);

/**
 * \fn int codeToDec (LSDC*, LSDC*, LSDC*)
 * \brief convertit un code de Huffman représenté dans une LSDC en nombre décimal
 * 
 * \param liste : LSDC, contenant la liste des indices des codes à écrire contenus dans le tableau de codes
 * \param codes : LSDC, contenant le tableau des codes représentant chaque caractère
 * \param code : LSDC, contenant un reste de code à convertir du précédent appel de la fonction
 * 
 * \return entier, valant un nombre décimal compris entre 0 et 255 généré à partir des 8 premiers bits des codes
 */

int codeToDec (LSDC*, LSDC*, LSDC*);

/**
 * \fn LSDC CellSuivante (LSDC*)
 * \brief Permet d'avoir l'adresse de la cellule suivante d'une LSDC
 * 
 * Retourne soit l'adresse de la cellule suivante d'une LSDC, soit NULL si cette cellule suivante est la première cellule de la LSDC
 * 
 * \param liste : LSDC, dont on veut avoir l'adresse suivante
 * 
 * \return LSDC, adresse de la cellule suivante, ou NULL si celle-ci est la première cellule de la liste
 */

LSDC CellSuivante (LSDC*);

/**
 * \fn LSDC copieListe (LSDC)
 * \brief Retourne une copie de la liste doublement chaînée
 * 
 * \param lsdc : LSDC, liste à copier
 * 
 * \return LSDC, la copie de lsdc
 */

LSDC copieListe (LSDC);

#endif