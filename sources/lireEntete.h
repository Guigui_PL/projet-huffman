/**
 * \file lireEntete.h
 * \brief Contient la signature de la fonction servant lire l'entête d'un fichier compressé avec le compresseur huff
 * \authors Guillaume Pérution-Kihli et Judicaël Russo
 */

#ifndef LIREENTETE_H
#define LIREENTETE_H
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>

/**
 * \fn int* lireEntete (FILE*, int*, int*)
 * \brief Lit l'entête du fichier compressé
 * 
 * \param fg : FILE*, adresse vers le flux de lecture du fichier compressé, le curseur doit être placé au début du fichier
 * \param nbCaracteresDiff : entier, nombre de caractères différents représentés par l'abre
 * \param nbCaracteresTotal : entier, nombre de caractères total que contiendra le fichier décompressé
 * 
 * \return tableau d'entiers, faisant la correspondance entre la valeur décimal des caractères et l'indice de leur feuille dans l'abre
 */

int* lireEntete (FILE*, int*, int*);

#endif
