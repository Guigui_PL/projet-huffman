/**
 * \file dehuff.c
 * \brief Contient la définition de la fonction main du décompresseur
 * \authors Guillaume Pérution-Kihli et Judicaël Russo
 */

#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include "Noeud.h"
#include "arbreHuff.h"
#include "codeHuff.h"
#include "lireEntete.h"
#include "lireCara.h"

/**
 * \fn main (int argc, char** argv)
 * \brief Fonction main du décompresseur
 */

int main (int argc, char** argv)
{
	// On vérifie la validité des arguments et la possibilité d'ouvrir le fichier à décompresser
	if (argc < 2)
	{
		fprintf(stderr, "Vous devez indiquer un fichier à décompresser ! \n Utilisation : dehuff fichier_à_décompresser \n");
		exit(EXIT_FAILURE);
	}

	FILE* fg = NULL;
	fg = fopen(argv[1], "r");

	if (fg == NULL) 
	{
		fprintf(stderr, "Impossible d'ouvrir %s\n", argv[1]);
		exit(EXIT_FAILURE);
	}
	// ---
	
	int nbCaracteresDiff, nbCaracteresTotal;
	int *correspondances = NULL;
	
	// Lecture de l'entête
	correspondances = lireEntete(fg, &nbCaracteresDiff, &nbCaracteresTotal);
	// ---

	// Lecture de l'abre de Huffman
	Noeud* arbre = NULL;
	if (nbCaracteresDiff > 1) arbre = lireArbre(fg, nbCaracteresDiff);
	// ---
	
	// Enfin, on lit les caractères dans le fichier, on les décode, et on les affiche dans la sortie standard
	lireCara (fg, arbre, correspondances, nbCaracteresTotal, nbCaracteresDiff);
	// ---

	// Désallocation de tout ce qui a été alloué dyamiquement
	free(correspondances);	
	correspondances = NULL;
	
	if (nbCaracteresDiff > 1) 
	{
		free(arbre);
		arbre = NULL;
	}
	// ---
	
	// Fermeture du flux de lecture du fichier
	fclose(fg);
	// ---
}
