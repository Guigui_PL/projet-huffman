/**
 * \file huff.c
 * \brief Contient la définition de la fonction main du compresseur
 * \authors Guillaume Pérution-Kihli et Judicaël Russo
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include "Noeud.h"
#include "probHuffman.h"
#include "arbreHuff.h"
#include "LSDC.h"
#include "codeHuff.h"
#include "writeHuff.h"

/**
 * \fn main (int argc, char** argv)
 * \brief Fonction main du compresseur
 */

int main (int argc, char** argv)
{
	// On déclare l'arbre et on initialise ses valeurs à -1
	Noeud arbre[511];
	initArbre(arbre, 511);
	// ---
	
	int nbCaracteresTotal;
	
	// On vérifie la validité des arguments et la possibilité de lire/écrire dans les différents fichiers
	if (argc < 3)
	{
		fprintf(stderr, "Erreur ! Arguments manquants !\n Utilisation : huff fichier_à_compresser fichier_destination\n");
		exit(EXIT_FAILURE);
	}
	
	FILE* fg = NULL;
	fg = fopen(argv[1], "r");

	if (fg == NULL) 
	{
		fprintf(stderr, "Impossible d'ouvrir %s\n", argv[1]);
		exit(EXIT_FAILURE);
	}

	FILE* fp = NULL;
	fp = fopen(argv[2], "r");
	
	char confirm[1];

	if (argc == 3 || strcmp(argv[3] , "-f") != 0)
	{
		if(fp != NULL)
		{
			do 
			{
				printf("Le fichier %s existe déjà, voulez-vous l'écraser [y/n]?\n", argv[2]);
				scanf("%c", confirm);
			} while (confirm[0] != 'n' && confirm[0] != 'y');
		}

		if(confirm[0] == 'n')
		{
			printf("Compression annulée, fin du programme. \n");
			return 0;
		}
	}

	fp = fopen(argv[2], "wb+");
	if (fp == NULL) 
	{
		fprintf(stderr, "Impossible d'ouvrir/écrire %s\n", argv[2]);
		exit(EXIT_FAILURE);
	}
	// ---

	// On calcule la distribution de chaque caractère dans le fichier à compresser et on l'affiche
	double *probabilite = distribution(fg, &nbCaracteresTotal);
	afficheDistrib(probabilite);
	// ---

	int nbCaracteresDiff;
	
	int* correspondances = (int*) malloc(256 * sizeof(int));
	if (correspondances == NULL)
	{
		fprintf(stderr, "Allocation impossible (variable correspondances, fonction main, fichier %s, ligne %i) \n", __FILE__, __LINE__);
		exit(EXIT_FAILURE);
	}
	for (int i = 0; i < 256; i++) correspondances[i] = -1;
	
	// On génère l'arbre de Huffman à l'aide de la distribution des caractères
	nbCaracteresDiff = arbreHuff(probabilite, arbre, correspondances);
	// ---
	
	// On génère les codes représentant chaque caractère et on les affiche
	LSDC* codes;
	codes = codeHuff(arbre, nbCaracteresDiff);
	afficherCodes(codes, correspondances, nbCaracteresDiff);
	// --- 
	
	// On écrit le fichier final
	int nbOctets = writeHuff(fp, fg, probabilite, arbre, codes, correspondances, nbCaracteresTotal, nbCaracteresDiff);
	// ---

	// Affichage dans le terminal du gain d'espace
	longueurMoyenne (correspondances, probabilite, codes, nbCaracteresDiff);
	printf("Taille originelle : %i; taille compressée : %i; gain : %.1f%c ! \n", nbCaracteresTotal, nbOctets, (double) (1-(double)nbOctets/nbCaracteresTotal)*100, 37);
	// ---
	
	// Fermeture des fichiers
	fclose(fp);
	fclose(fg);
	// ---

	// Désallocation de tout ce qui a été alloué dyamiquement
	desallocTCB (codes, nbCaracteresDiff);
	
	free(probabilite);
	probabilite = NULL;
	
	free(correspondances);
	correspondances = NULL;
	// ---
}
