#!/usr/bin/env python
#coding: utf8

import time  
import subprocess
import os
import sys
f = open(os.devnull, 'w')
      

depart = "test.docx"
destination = "docx"
executions = 50

assert os.path.exists(depart) & os.path.isfile(depart)

print 'Fichier testé : ', depart
print 'Nombre d\'exécutions demandé : ', executions
print ''

print 'Lancement du test pour huff (l\'exécutable du projet) ...'

start_time = time.time()  

for i in range (0, executions) :
	subprocess.check_call(["./huff", depart, destination, "-f"], stdout=f, shell=False)
	
interval1 = time.time() - start_time  

print 'Temps total en secondes:', interval1  
print 'Temps moyen : ', interval1/executions
print ''

print 'Lancement du test pour huf (l\'exécutable d\'exemple) ...'

start_time = time.time()  

for i in range (0, executions) :
	subprocess.check_call(["./huf", depart, destination], stdout=f, shell=False)
	
interval2 = time.time() - start_time  

print 'Temps total en secondes:', interval2
print 'Temps moyen : ', interval2/executions
print ''

if interval2/interval1 >= 1:
  print 'huff est ', interval2/interval1, 'fois plus rapide que huf !'
else:
  print 'huf est ', interval1/interval2, 'fois plus rapide que huff !'