var searchData=
[
  ['lirearbre',['lireArbre',['../arbre_huff_8c.html#a7e0d1620af449b7d9b2a789eba439659',1,'lireArbre(FILE *fg, int nbCaracteresDiff):&#160;arbreHuff.c'],['../arbre_huff_8h.html#a7f7228d283a9e4194787315695d0c668',1,'lireArbre(FILE *, int):&#160;arbreHuff.c']]],
  ['lirecara',['lireCara',['../lire_cara_8c.html#a666a4c43f59579574cef19235dd5ee1d',1,'lireCara(FILE *fg, Noeud *arbre, int *correspondances, int nbCaracteresTotal, int nbCaracteresDiff):&#160;lireCara.c'],['../lire_cara_8h.html#a89708891c0a8c4b348cea91892b1e8bf',1,'lireCara(FILE *, Noeud *, int *, int, int):&#160;lireCara.c']]],
  ['lirecara_2ec',['lireCara.c',['../lire_cara_8c.html',1,'']]],
  ['lirecara_2eh',['lireCara.h',['../lire_cara_8h.html',1,'']]],
  ['lireentete',['lireEntete',['../lire_entete_8c.html#a71e6a7a8725e20e15a6559d22d6886db',1,'lireEntete(FILE *fg, int *nbCaracteresDiff, int *nbCaracteresTotal):&#160;lireEntete.c'],['../lire_entete_8h.html#a244a01df788221ee377af3c7fcc57cac',1,'lireEntete(FILE *, int *, int *):&#160;lireEntete.c']]],
  ['lireentete_2ec',['lireEntete.c',['../lire_entete_8c.html',1,'']]],
  ['lireentete_2eh',['lireEntete.h',['../lire_entete_8h.html',1,'']]],
  ['longueurmoyenne',['longueurMoyenne',['../code_huff_8c.html#ad299070de2d3f0798b9c6c36c372dd31',1,'longueurMoyenne(int *correspondances, double *distrib, LSDC *codes, int nbCaracteresDiff):&#160;codeHuff.c'],['../code_huff_8h.html#ac7c4e8256767b3d970c28607f6cb5cd3',1,'longueurMoyenne(int *, double *, LSDC *, int):&#160;codeHuff.c']]],
  ['lsdc',['LSDC',['../_l_s_d_c_8h.html#a10fec00b451b8ad8577433bc419c3b76',1,'LSDC.h']]],
  ['lsdc_2ec',['LSDC.c',['../_l_s_d_c_8c.html',1,'']]],
  ['lsdc_2eh',['LSDC.h',['../_l_s_d_c_8h.html',1,'']]]
];
